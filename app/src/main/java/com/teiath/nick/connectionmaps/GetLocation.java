package com.teiath.nick.connectionmaps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import java.io.Serializable;

import static java.lang.String.valueOf;

/**
 * Created by Nick on 28-May-17.
 */

public class GetLocation extends FragmentActivity implements
        Serializable,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private static final int REQUEST_ACCESS_FINE_LOCATION = 16;
    Location mLastLocation;
    GoogleApiClient mGoogleApiClient;
    String mLatitudeText;
    String mLongitudeText;
    Context context;
    MainActivity activity;

    public GetLocation(Context context, MainActivity activity){
        this.context = context;
        this.activity = activity;

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Snackbar.make(activity.findViewById(android.R.id.content), "Permission Granted", Snackbar.LENGTH_LONG)
                            .setActionTextColor(Color.RED)
                            .show();
                }
                break;

            default:
                break;
        }
    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION);
            return;
        }
        else{
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            if (mLastLocation != null) {
                mLatitudeText = valueOf(mLastLocation.getLatitude());
                mLongitudeText = valueOf(mLastLocation.getLongitude());
                Snackbar.make(activity.findViewById(android.R.id.content), "Location found", Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.RED)
                        .show();

            }
        }

    }


    @Override
    public void onConnectionSuspended(int i) {

    }


    public String getmLatitudeText() {
        return mLatitudeText;
    }

    public String getmLongitudeText() {
        return mLongitudeText;
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


}
