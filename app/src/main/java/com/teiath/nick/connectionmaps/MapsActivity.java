package com.teiath.nick.connectionmaps;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import static java.lang.String.valueOf;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;
    private static final int REQUEST_ACCESS_FINE_LOCATION = 16;
    Location mLastLocation;
    GoogleApiClient mGoogleApiClient;
    String mLatitudeText;
    String mLongitudeText;
    SQLiteDatabase db;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        MainActivity activity;
        if (extras != null) {
            mLatitudeText = extras.getString("lat");
            mLongitudeText = extras.getString("lng");
        }

        db = Database.getInstance(this).getWritableDatabase();


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if (mLongitudeText != null && mLatitudeText != null) {
            LatLng topical = new LatLng(Float.parseFloat(mLatitudeText), Float.parseFloat(mLongitudeText));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(topical));
            mMap.animateCamera( CameraUpdateFactory.zoomTo( 17.0f ) );
        }
        else {
            mMap.animateCamera( CameraUpdateFactory.zoomTo( 5.0f ) );
            Snackbar snack = Snackbar.make(findViewById(android.R.id.content), "Cannot obtain current location", Snackbar.LENGTH_LONG);
            View view = snack.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snack.show();

        }

        Button cosmote = (Button) findViewById(R.id.cosmote);
        cosmote.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                showPins("cosmote");
            }
        });

        Button vodafone = (Button) findViewById(R.id.vodafone);
        vodafone.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                showPins("vodafone");
            }
        });

        Button wind = (Button) findViewById(R.id.wind);
        wind.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                showPins("wind");
            }
        });

        }

        public void showPins(String provider){

            Cursor cursor = db.rawQuery("SELECT * FROM pins WHERE provider LIKE '%"+provider+"%'", null);
            mMap.clear();
            if (cursor.moveToFirst()) {
                do {
                    LatLng place = new LatLng(cursor.getFloat(1), cursor.getFloat(2));
                    Float color = BitmapDescriptorFactory.HUE_RED;
                    if (cursor.getString(0).equals("H ") || cursor.getString(0).equals("H+") || cursor.getString(0).equals("3G"))
                    {color = BitmapDescriptorFactory.HUE_CYAN;}
                    else if (cursor.getString(0).equals("2G") || cursor.getString(0).equals("G ") || cursor.getString(0).equals("E "))
                    {color = BitmapDescriptorFactory.HUE_ORANGE;}
                    else if (cursor.getString(0).equals("4G"))
                    {color = BitmapDescriptorFactory.HUE_GREEN;}
                    else {color = BitmapDescriptorFactory.HUE_RED;}
                    mMap.addMarker(new MarkerOptions().position(place)
                            .title("Type: " + cursor.getString(0)+ " / Strength: " + cursor.getString(3))
                            .icon(BitmapDescriptorFactory
                                    .defaultMarker(color)));
                }
                while(cursor.moveToNext());
            }
        }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
}


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
