package com.teiath.nick.connectionmaps;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements Serializable, ActivityCompat.OnRequestPermissionsResultCallback{

    private static final int REQUEST_ACCESS_COARSE_LOCATION = 15;
    final Context context = this;
    MainActivity activity = this;
    GetLocation mLoc ;
    Database db = Database.getInstance(this);
    String lng;
    String lat;



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_COARSE_LOCATION:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {

                   Snackbar.make(findViewById(android.R.id.content), "Your signal type is: " + getNetworkType(context)+ " ", Snackbar.LENGTH_LONG)
                           .setActionTextColor(Color.WHITE)
                           .show();
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLoc = new GetLocation(context,activity);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button exitButton = (Button) findViewById(R.id.exitButton);
        exitButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                System.exit(1);
            }
        });

        Button updateButton = (Button) findViewById(R.id.refreshButton);
        updateButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
               try {
                    String result = getNetworkType(context);
                    char[] charArray = result.toCharArray();
                    StringBuilder provider = new StringBuilder();
                    for (int i= 33; i<result.length(); i++){provider.append(charArray[i]);}
                    if (mLoc.getmLatitudeText() != null && mLoc.getmLongitudeText() != null) {
                        db.insertEntry(new StringBuilder().append(charArray[0]).append(charArray[1]).toString(),
                                Float.parseFloat(mLoc.getmLatitudeText()),
                                Float.parseFloat(mLoc.getmLongitudeText()),
                                new StringBuilder().append(charArray[31]).append(charArray[32]).toString(),
                                provider.toString());

                    }
                    else{
                        Snackbar.make(findViewById(android.R.id.content), "Location not available ", Snackbar.LENGTH_LONG)
                            .setActionTextColor(Color.WHITE)
                            .show();
                    }
                }
                catch (IndexOutOfBoundsException e1){Snackbar.make(findViewById(android.R.id.content), "-Location not available", Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.WHITE)
                        .show();}

            }
        });

        Button testConn = (Button) findViewById(R.id.testConn);
        testConn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                int permissionCheck = ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ACCESS_COARSE_LOCATION);
                } else {
                    try{

                    Snackbar.make(findViewById(android.R.id.content), "Your signal type is: " + getNetworkType(context) + " ", Snackbar.LENGTH_LONG)
                           .setActionTextColor(Color.WHITE)
                           .show();
                    }
                    catch(IndexOutOfBoundsException e){Snackbar.make(findViewById(android.R.id.content), "Can't find network info", Snackbar.LENGTH_LONG)
                            .setActionTextColor(Color.WHITE)
                            .show();}}
                    }
        });


        Button mapButton = (Button) findViewById(R.id.mapButton);
        mapButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MapsActivity.class);
                lng = mLoc.getmLongitudeText();
                lat = mLoc.getmLatitudeText();
                i.putExtra("lng", lng);
                i.putExtra("lat", lat);
                startActivity(i);

            }
        });
    }

    public String getNetworkType(Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = mTelephonyManager.getNetworkType();
        String provider = mTelephonyManager.getNetworkOperatorName();
        CellInfo info = mTelephonyManager.getAllCellInfo().get(0);
        Integer asuLevel;
        if (info instanceof CellInfoGsm){
            asuLevel = ((CellInfoGsm) info).getCellSignalStrength().getAsuLevel();
                    }
        else if (info instanceof CellInfoWcdma){
            asuLevel = ((CellInfoWcdma)info).getCellSignalStrength().getAsuLevel();
        }
        else if (info instanceof CellInfoCdma){
            asuLevel = ((CellInfoCdma)info).getCellSignalStrength().getAsuLevel();
        }
        else{
            asuLevel = ((CellInfoLte)info).getCellSignalStrength().getAsuLevel();
        }
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "G " + " and your signal strength is " +  asuLevel.toString() +" " + "provider: "+ provider;
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "E " + " and your signal strength is " +  asuLevel.toString() +" " + "provider: "+ provider;
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "3G" + " and your signal strength is " +  asuLevel.toString() +" " + "provider: "+ provider;
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G" + " and your signal strength is " +  asuLevel.toString() +" " + "provider: "+ provider;
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "3G" + " and your signal strength is " +  asuLevel.toString() +" " + "provider: "+ provider;
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "H+" + " and your signal strength is " +  asuLevel.toString() +" " + "provider: "+ provider;
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "H+" + " and your signal strength is " +  asuLevel.toString() +" " + "provider: "+ provider;
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "H+" + " and your signal strength is " +  asuLevel.toString() +" " + "provider: "+ provider;
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "H+" + " and your signal strength is " +  asuLevel.toString() +" " + "provider: "+ provider;
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "4G" + " and your signal strength is " +  asuLevel.toString() +" " + "provider: "+ provider;
            default:
                return "                              Unknown                   ";
        }
    }

}
