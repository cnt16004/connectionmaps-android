package com.teiath.nick.connectionmaps;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.Serializable;

/**
 * Created by Nick on 28-May-17.
 */

public class Database  extends SQLiteOpenHelper implements Serializable {

    private static Database sInstance;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "pinsDB";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE pins ( type TEXT, lng FLOAT, lat FLOAT, strength TEXT, provider TEXT, primary key (lng,lat,provider) )";
        db.execSQL(CREATE_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + "pins");
// Creating tables again
        onCreate(db);
    }

    public static synchronized Database getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new Database(context);
        }
        return sInstance;
    }

    public void insertEntry(String type, Float lng, Float lat, String strength, String provider){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("type", type);
        values.put("lng", lng);
        values.put("lat", lat);
        values.put("strength", strength);
        values.put("provider", provider);

        db.insertWithOnConflict("pins", null, values,SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    };

}
